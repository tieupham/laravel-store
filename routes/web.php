<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', 'front\HomeController@index')->name('home');

Route::post('search', 'front\HomeController@postSearch');

Route::get('search', 'front\HomeController@getSearch');

Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout');

Route::get('product/{id}', 'front\ProductController@index')->name('product');

Route::get('category/{id}', 'front\CategoryController@index');

Route::group(['prefix' => 'cart'], function () {
    Route::get('add/{id}', 'front\CartController@addToCart');

    Route::get('shopping-cart', 'front\CartController@getCart');

    Route::get('checkout', 'front\CartController@getCheckout');

    Route::get('history', 'front\CartController@getHistory');

    Route::post('checkout', 'front\CartController@postCheckout');

    Route::post('delete-cart-item', 'front\CartController@deleteCartItem');

    Route::post('change-item-count', 'front\CartController@updateCartItem');

    Route::post('cancel-order', 'front\CartController@cancelOrder');
});

Route::group(['prefix' => 'admin', 'middleware' => ['auth', 'admin']], function () {
    Route::group(['prefix' => 'category'], function () {
        Route::get('/', 'admin\CategoryController@index');

        Route::get('add', 'admin\CategoryController@add');

        Route::post('add', 'admin\CategoryController@saveAdd');

        Route::get('edit/{id}', 'admin\CategoryController@edit');

        Route::post('edit/{id}', 'admin\CategoryController@saveEdit');

        Route::get('delete/{id}', 'admin\CategoryController@delete');
    });

    Route::group(['prefix' => 'producer'], function () {
        Route::get('/', 'admin\ProducerController@index');

        Route::get('add', 'admin\ProducerController@add');

        Route::post('add', 'admin\ProducerController@saveAdd');

        Route::get('edit/{id}', 'admin\ProducerController@edit');

        Route::post('edit/{id}', 'admin\ProducerController@saveEdit');

        Route::get('delete/{id}', 'admin\ProducerController@delete');
    });

    Route::group(['prefix' => 'product'], function () {
        Route::get('/', 'admin\ProductController@index');

        Route::get('add', 'admin\ProductController@add');

        Route::post('add', 'admin\ProductController@saveAdd');

        Route::get('edit/{id}', 'admin\ProductController@edit');

        Route::post('edit/{id}', 'admin\ProductController@saveEdit');

        Route::get('delete/{id}', 'admin\ProductController@delete');
    });
    Route::group(['prefix' => 'order'], function () {
        Route::get('/', 'admin\OrderController@index');
        Route::get('detail/{id}', 'admin\OrderController@detailOrder');
        Route::post('confirm-order', 'admin\OrderController@confirmOrder');
    });
});
