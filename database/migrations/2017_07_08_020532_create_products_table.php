<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('category_id')->unsigned();
            $table->foreign('category_id')->references('id')->on('categories');
            $table->integer('producer_id')->unsigned();
            $table->foreign('producer_id')->references('id')->on('producers');
            $table->string('name', 100);
            $table->text('description');
            $table->text('image')->nullable();
            $table->bigInteger('price')->nullable();
            $table->float('sale')->default(0);
            $table->bigInteger('quantity');
            $table->timestamps();
            $table->tinyInteger('deleted')->default(0)->comment('0: active,1: deleted');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
