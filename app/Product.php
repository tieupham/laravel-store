<?php

namespace App;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Product extends Model
{
    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope('deleted', function (Builder $builder) {
            $builder->where('products.deleted', '=', 0);
        });
    }

    public function category()
    {
        return $this->belongsTo('App\Category');
    }

    public function producer()
    {
        return $this->belongsTo('App\Category');
    }

    public function orderDetail(){
        return $this->hasMany('App\Order_detail');
    }

    public function getAccessories()
    {
        return DB::table('products')
            ->select('products.*')
            ->join('categories', 'categories.id', '=', 'products.category_id')
            ->where('categories.parent_id', 4)
            ->take(8)
            ->orderBy('products.price', 'asc')
            ->get();
    }
}
