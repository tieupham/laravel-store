<?php

namespace App;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $table = 'orders';

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope('deleted', function (Builder $builder) {
            $builder->where('deleted', '=', 0);
        });
    }

    public function orderDetail(){
        return $this->hasMany('App\Order_detail');
    }

    public function user(){
        return $this->belongsTo('App\User');
    }
}
