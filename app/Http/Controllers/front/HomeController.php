<?php

namespace App\Http\Controllers\front;

use App\Category;
use App\Http\Controllers\Controller;
use App\Product;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
//        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = array();
        $product = new Product();
        $data['css'][] = 'css/home.css';
        $data['list_products'] = Product::orderBy('id', 'desc')->take(12)->get();
        $data['list_accessories'] = Category::where('parent_id', 1)->get();
        $data['accessory_products'] = $product->getAccessories();
        return view('front.home.index', $data);
    }

    public function postSearch(Request $request)
    {
        $data['css'][] = 'css/search.css';
        $this->validate($request, [
            'key_word' => 'required',
        ]);

        $products = Product::join('categories', 'categories.id', '=', 'products.category_id')
            ->join('producers', 'producers.id', '=', 'products.producer_id')
            ->where('products.name', 'LIKE', '%' . $request->key_word . '%')
            ->orWhere('categories.name', 'LIKE', '%' . $request->key_word . '%')
            ->orWhere('categories.name', 'LIKE', '%' . $request->key_word . '%')
            ->select('products.*')
            ->get();
        $data['products'] = $products;
        $data['key_word'] = $request->key_word;

        return view('front.product.postSearch', $data);
    }

    public function getSearch(){
        $data['css'][] = 'css/search.css';
        return view('front.product.getSearch',$data);
    }
}
