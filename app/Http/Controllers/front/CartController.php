<?php

namespace App\Http\Controllers\front;

use App\Cart;
use App\Order;
use App\Order_detail;
use App\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Response;
use Session;

class CartController extends Controller
{
    public function addToCart(Request $request, $id)
    {
        $product = Product::find(htmlentities($id));
        if ($product) {
            if ($product->sale > 0) {
                $product->price = $product->price - ($product->price * $product->sale) / 100;
            }
            $oldCart = Session::has('cart') ? Session::get('cart') : null;
            $cart = new Cart($oldCart);
            $cart->add($product, $product->id);
            $request->session()->put('cart', $cart);
            return redirect()->route('product', ['id' => $id]);
        }
    }

    public function getCart()
    {
        $data = array();
        $data['css'][] = 'css/cart.css';
        $data['js'][] = 'js/cart.js';
        if (!Session::has('cart')) {
            return view('front.cart.list');
        }
        $oldCart = Session::get('cart');
        $cart = new Cart($oldCart);
        $data['products'] = $cart->items;
        $data['totalPrice'] = $cart->totalPrice;
        return view('front.cart.list', $data);
    }

    public function deleteCartItem(Request $request)
    {
        $data_return = array(
            'status'  => false,
            'message' => 'data error',
        );
        if (isset($request->key) && is_numeric($request->key)) {
            $oldCart = Session::has('cart') ? Session::get('cart') : null;
            if ($oldCart) {
                $product = Product::find($request->key);
                unset($oldCart->items[$request->key]);
                $oldCart->totalQty--;
                $oldCart->totalPrice -= $product->price - ($product->price * $product->sale) / 100;
                if (count($oldCart->items) == 0) {
                    $request->session()->forget('cart');
                }
                $data_return['status'] = true;
                $data_return['message'] = 'update success';
            }
        }
        return Response::json($data_return);
    }

    public function updateCartItem(Request $request)
    {
        $data_return = array(
            'status'  => false,
            'message' => 'data error',
        );
        if (isset($request->key, $request->new_count) && is_numeric($request->key)) {
            $oldCart = Session::has('cart') ? Session::get('cart') : null;
            if ($oldCart) {
                if ($request->new_count > 0) {
                    $product = Product::find($request->key);
                    $oldCart->totalQty += $request->new_count - $oldCart->items[$request->key]['qty'];
                    $oldCart->totalPrice += ($product->price - ($product->price * $product->sale) / 100)
                        * $request->new_count - $oldCart->items[$request->key]['price'];
                    $oldCart->items[$request->key]['qty'] = $request->new_count;
                    $oldCart->items[$request->key]['price'] = ($product->price - ($product->price * $product->sale) / 100)
                        * $request->new_count;
                } else {
                    $oldCart->totalQty -= $oldCart->items[$request->key]['qty'];
                    $oldCart->totalPrice -= $oldCart->items[$request->key]['price'];
                    unset($oldCart->items[$request->key]);
                    if (count($oldCart->items) == 0) {
                        $request->session()->forget('cart');
                    }
                }
                $data_return['status'] = true;
                $data_return['message'] = 'update success';
            }
        }
        return Response::json($data_return);
    }

    public function getCheckout()
    {
        return view('front.cart.checkout');
    }

    public function postCheckout(Request $request)
    {
        $this->validate($request,
            [
                'user_name' => 'required',
                'phone'     => 'required|min:10',
                'address'   => 'required',
            ]
        );
        $oldCart = Session::get('cart');
        $order = new Order();
        $order->order_code = 'OD' . time();
        $order->total_price = $oldCart->totalPrice;
        if (Auth::check()) {
            $order->user_id = Auth::user()->id;
        } else {
            $order->user_name = $request->user_name;
            $order->address = $request->address;
            $order->phone = $request->phone;
        }
        DB::beginTransaction();
        if ($order->save()) {
            $data_order_detail_insert = array();
            foreach ($oldCart->items as $key => $value) {
                $order_detail['order_id'] = $order['id'];
                $order_detail['product_id'] = $key;
                $order_detail['price'] = $value['price'];
                $order_detail['count'] = $value['qty'];
                array_push($data_order_detail_insert, $order_detail);
            }
            if (Order_detail::insert($data_order_detail_insert)) {
                $request->session()->forget('cart');
                DB::commit();
                return redirect('/')->with('msg', 'Đặt hàng thành công. Hãy tiếp tục mua hàng tại Store nào.');
            } else {
                DB::rollBack();
            }
        }
    }

    public function getHistory()
    {
        $data = array();
        $data['css'][] = 'css/cart.css';
        $data['js'][] = 'js/cart.js';
        if (Auth::check()) {
            $data['orders'] = Order::where('user_id', Auth::user()->id)->get();
        }
        return view('front.cart.history', $data);
    }

    public function cancelOrder(Request $request)
    {
        $data_return = array(
            'status'  => false,
            'message' => 'data error',
        );
        if (isset($request->order_id) && is_numeric($request->order_id)) {
            DB::beginTransaction();
            $order = Order::where('id', $request->order_id)
                ->update(['deleted' => 1]);
            $order_detail = Order_detail::where('order_id',$request->order_id)
                ->update(['deleted' => 1]);
            if($order && $order_detail){
                DB::commit();
                $data_return['status'] = true;
                $data_return['message'] = 'Cancel order successfully.';
            }else{
                DB::rollBack();
                $data_return['message'] = 'Cancel order fail.';
            }
        }
        return Response::json($data_return);
    }
}
