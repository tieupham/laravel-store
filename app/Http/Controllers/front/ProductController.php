<?php

namespace App\Http\Controllers\front;

use App\Product;
use App\Http\Controllers\Controller;

class ProductController extends Controller
{
    public function index($id)
    {
        $data = array();
        $data['css'][] = 'css/product.css';
        $product = Product::find(htmlentities($id));
        if ($product) {
            $data['product'] = $product;
            $data['product_involve'] = Product::where('category_id', $product->category_id)
                ->orWhere('producer_id', $product->producer_id)
                ->take(5)
                ->get();
        }
        return view('front.product.index', $data);
    }
}
