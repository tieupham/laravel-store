<?php

namespace App\Http\Controllers\front;

use App\Category;
use App\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CategoryController extends Controller
{
    public function index($id)
    {
        $data = Array();
        $data['css'][] = 'css/category.css';
        $data['js'][] = 'js/category.js';
        $id = htmlentities($id);
        if (is_numeric($id)) {
            $category = Category::find($id);
            if ($category->parent_id != null) {
                $data['list_cat_child'] = Category::where('parent_id', $category->parent_id)->get();
            } else {
                $data['list_cat_child'] = Category::where('parent_id', $id)->get();
            }
            $data['list_products'] = Product::join('categories', 'categories.id', '=', 'products.category_id')
                ->where('categories.id', $id)
                ->orWhere('categories.parent_id', $id)
                ->select('products.*')
                ->paginate(5);
            $data['category_id'] = $id;
            $data['category_name'] = $category;
        }
        return view('front.category.index', $data);
    }
}
