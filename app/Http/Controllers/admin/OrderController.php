<?php

namespace App\Http\Controllers\admin;

use App\Order;
use App\Order_detail;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Response;

class OrderController extends Controller
{
    public function index()
    {
        $data = Array();
        $data['js'][] = 'js/order.js';
        $data['list_order'] = Order::all();
        return view('admin.order.list', $data);
    }

    public function detailOrder($order_id)
    {
        $data = Array();
        if ($order_id && is_numeric($order_id)) {
            $order = Order::find($order_id);
            if (!empty($order) && count($order) > 0) {
                $data['order_details'] = Order_detail::where('order_id', $order_id)->get();
                $data['order'] = $order;
            }
        }
        return view('admin.order.detail', $data);
    }

    public function confirmOrder(Request $request)
    {
        $data_return = Array(
            'status'  => false,
            'message' => "Xác nhận thất bại",
        );
        if (isset($request->order_id) && is_numeric(htmlentities($request->order_id))) {
            if ($order = Order::where('id', $request->order_id)
                ->update(['status' => 1])
            ) {
                $data_return['status'] = true;
                $data_return['message'] = "Đã xác nhận đơn hàng";
            }
        }
        return Response::json($data_return);
    }
}
