<?php

namespace App\Http\Controllers\admin;

use App\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CategoryController extends Controller
{
    public function index()
    {
        $data = array();
        $data['categories'] = Category::all();
        return view('admin.category.list', $data);
    }

    public function add()
    {
        $data = array();
        $data['categories'] = Category::where('parent_id', 0)->get();
        return view('admin.category.add', $data);
    }

    public function saveAdd(Request $request)
    {
        $this->validate($request,
            [
                'name' => 'required|min:3|max:100',
            ]
        );
        $cat = new Category();
        $cat->name = $request->name;
        $cat->parent_id = isset($request->parent_id) ? $request->parent_id : 0;
        $cat->save();
        return redirect('admin/category')->with('msg', 'Add category ' . $cat->name . ' success');
    }

    public function edit($id)
    {
        $data = array();
        $data['categories'] = Category::where('parent_id', 0)->get();
        $data['category'] = Category::find($id);
        return view('admin.category.edit', $data);
    }

    public function saveEdit(Request $request, $id)
    {
        $this->validate($request,
            [
                'name' => 'required|min:3|max:100',
            ]
        );
        $cat = Category::find($id);
        $old_name = $cat->name;
        $cat->name = $request->name;
        $cat->parent_id = isset($request->parent_id) ? $request->parent_id : 0;
        $cat->save();
        return redirect('admin/category')->with('msg', 'Edit category ' . $old_name . ' to ' . $cat->name . ' success');
    }

    public function delete($id)
    {
        $cat = Category::find($id);
        $cat->deleted = 1;
        $cat->save();
        return redirect('admin/category')->with('msg', 'Delete category ' . $cat->name . ' success');
    }
}
