<?php

namespace App\Http\Controllers\admin;

use App\Category;
use App\Producer;
use App\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProductController extends Controller
{
    public function index()
    {
        $data = array();
        $data['products'] = Product::all();
        return view('admin.product.list', $data);
    }

    public function add()
    {
        $data = array();
        $data['categories'] = Category::all();
        $data['producers'] = Producer::all();
        return view('admin.product.add', $data);
    }

    public function saveAdd(Request $request)
    {
        $this->validate($request,
            [
                'name'        => 'required',
                'description' => 'required',
                'price'       => 'required|numeric|min:0',
                'sale'        => 'required|numeric|min:0|max:70',
                'quantity'    => 'required|min:0|numeric',
            ]
        );
        $pro = new Product();
        $pro->name = $request->name;
        $pro->category_id = $request->category_id;
        $pro->producer_id = $request->producer_id;
        $pro->description = $request->description;
        $pro->price = $request->price;
        $pro->sale = $request->sale;
        $pro->quantity = $request->quantity;

        if ($request->hasFile('image')) {
            $file = $request->file('image');
            $type = $file->getClientOriginalExtension();
            $pro->image = base64_encode(file_get_contents($file));
            $extension = Array('jpg', 'jpeg', 'png', 'gif');
            if (!in_array($type, $extension)) {
                return redirect('admin/category/add')->with('msg', 'File image is not valid');
            }
        } else {
            $pro->image = "";
        }
        $pro->save();
        return redirect('admin/product')->with('msg', 'Add product ' . $pro->name . ' success');
    }

    public function edit($id)
    {
        $data = array();
        $data['categories'] = Category::all();
        $data['producers'] = Producer::all();
        $data['product'] = Product::find($id);
        return view('admin.product.edit', $data);
    }

    public function saveEdit(Request $request, $id)
    {
        $this->validate($request,
            [
                'name'        => 'required',
                'description' => 'required',
                'price'       => 'required|numeric|min:0',
                'sale'        => 'required|numeric|min:0|max:70',
                'quantity'    => 'required|min:0|numeric',
            ]
        );
        $pro = Product::find($id);
        $old_name = $pro->name;
        $pro->name = $request->name;
        $pro->category_id = $request->category_id;
        $pro->producer_id = $request->producer_id;
        $pro->description = $request->description;
        $pro->price = $request->price;
        $pro->sale = $request->sale;
        $pro->quantity = $request->quantity;

        if ($request->hasFile('image')) {
            $file = $request->file('image');
            $type = $file->getClientOriginalExtension();
            $pro->image = base64_encode(file_get_contents($file));
            $extension = Array('jpg', 'jpeg', 'png', 'gif');
            if (!in_array($type, $extension)) {
                return redirect('admin/category/add')->with('msg', 'File image is not valid');
            }
        }
        $pro->save();
        return redirect('admin/product')->with('msg', 'Edit product ' . $old_name . ' to ' . $pro->name . ' success');
    }

    public function delete($id)
    {
        $pro = Product::find($id);
        $pro->deleted = 1;
        $pro->save();
        return redirect('admin/product')->with('msg', 'Delete product ' . $pro->name . ' success');
    }
}
