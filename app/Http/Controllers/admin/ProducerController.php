<?php

namespace App\Http\Controllers\admin;

use App\Producer;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProducerController extends Controller
{
    public function index()
    {
        $data = array();
        $data['producers'] = Producer::all();
        return view('admin.producer.list', $data);
    }

    public function add()
    {
        $data = array();
        return view('admin.producer.add', $data);
    }

    public function saveAdd(Request $request)
    {
        $this->validate($request,
            [
                'name' => 'required|min:2|max:100',
            ]
        );
        $pro = new Producer();
        $pro->name = $request->name;
        $pro->save();
        return redirect('admin/producer')->with('msg', 'Add producer ' . $pro->name . ' success');
    }

    public function edit($id)
    {
        $data = array();
        $data['producer'] = Producer::find($id);
        return view('admin.producer.edit', $data);
    }

    public function saveEdit(Request $request, $id)
    {
        $this->validate($request,
            [
                'name' => 'required|min:2|max:100',
            ]
        );
        $pro = Producer::find($id);
        $old_name = $pro->name;
        $pro->name = $request->name;
        $pro->save();
        return redirect('admin/producer')->with('msg', 'Edit producer ' . $old_name . ' to ' . $pro->name . ' success');
    }

    public function delete($id)
    {
        $pro = Producer::find($id);
        $pro->deleted = 1;
        $pro->save();
        return redirect('admin/producer')->with('msg', 'Delete producer ' . $pro->name . ' success');
    }
}
