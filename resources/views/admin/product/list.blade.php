@extends('admin.layout.index')
@section('content')
    <!-- Page Content -->
    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Product
                        <small>List product</small>
                    </h1>
                    @if(session('msg'))
                        <div class="alert alert-success">
                            {{session('msg')}}
                        </div>
                    @endif
                </div>
                @if(isset($products) && count($products)>0)
                    <div style="width: 100%; margin: 0;overflow: auto">
                        <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                            <thead>
                            <tr align="center">
                                <th>ID</th>
                                <th>Name</th>
                                <th>Category</th>
                                <th>Producer</th>
                                <th>Description</th>
                                <th>Image</th>
                                <th>Price</th>
                                <th>Sale</th>
                                <th>Quantity</th>
                                <th>Created at</th>
                                <th>Delete</th>
                                <th>Edit</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($products as $item)
                                <tr class="odd gradeX" align="center">
                                    <td>{{$item->id}}</td>
                                    <td>{{$item->name}}</td>
                                    <td>{{$item->category->name}}</td>
                                    <td>{{$item->producer->name}}</td>
                                    <td>{{substr($item->description, 0, 100)}}...</td>
                                    <td>
                                        <img src="data:image/png;base64,{{$item->image}}" alt="{{$item->name}}"
                                             height="80px"/>
                                    </td>
                                    <td>{{number_format($item->price)}}</td>
                                    <td>{{$item->sale}} %</td>
                                    <td>{{number_format($item->quantity)}}</td>
                                    <td>{{date('d/m/Y',strtotime($item->created_at))}}</td>
                                    <td class="center"><i class="fa fa-trash-o  fa-fw"></i>
                                        <a href="{{url('admin/product/delete',$item->id)}}"
                                           onclick="return confirm('Are you sure you want to delete this product?');">
                                            Delete
                                        </a>
                                    </td>
                                    <td class="center"><i class="fa fa-pencil fa-fw"></i>
                                        <a href="{{url('admin/product/edit',$item->id)}}">
                                            Edit
                                        </a>
                                    </td>
                                </tr>
                            </tbody>
                            @endforeach
                        </table>
                    </div>
                @else
                    <h2>Don't have any product</h2>
                @endif
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </div>
    <!-- /#page-wrapper -->

@stop