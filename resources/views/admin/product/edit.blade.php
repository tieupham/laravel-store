@extends('admin.layout.index')
@section('content')
    <!-- Page Content -->
    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row">
                @if($product)
                    <div class="col-lg-12">
                        <h1 class="page-header">Product
                            <small>{{$product->name}}</small>
                        </h1>
                    </div>
                    <!-- /.col-lg-12 -->
                    <div class="col-lg-7" style="padding-bottom:120px">
                        <form action="{{url('admin/product/edit',$product->id)}}" method="POST"
                              enctype="multipart/form-data">
                            <input type="hidden" name="_token" value="{{csrf_token()}}">
                            @if(count($categories)>0)
                                <div class="form-group">
                                    <label>Category</label>
                                    <select name="category_id" class="form-control">
                                        @foreach($categories as $cat)
                                            <option {{$cat->id == $product->category_id ? "selected":""}} value="{{$cat->id}}">
                                                {{$cat->name}}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                            @endif
                            @if(count($producers)>0)
                                <div class="form-group">
                                    <label>Producer</label>
                                    <select name="producer_id" class="form-control">
                                        @foreach($producers as $pro)
                                            <option {{$pro->id == $product->producer_id ? "selected":""}} value="{{$pro->id}}">
                                                {{$pro->name}}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                            @endif
                            <div class="form-group">
                                <label>Product name</label>
                                <input class="form-control" name="name" value="{{$product->name}}" required/>
                            </div>
                            <div class="form-group">
                                <label>Description</label>
                                <textarea class="form-control" name="description" rows="3"
                                          required>{{$product->description}}</textarea>
                            </div>
                            <div class="form-group">
                                <label>Image</label>
                                <input type="file" name="image"/>
                                <img src="data:image/png;base64,{{$product->image}}" alt="{{$product->name}}"
                                     height="100px"/>
                            </div>
                            <div class="form-group">
                                <label>Price</label>
                                <input class="form-control" type="number" name="price" value="{{$product->price}}"
                                       required/>
                            </div>
                            <div class="form-group">
                                <label>Sale(%)</label>
                                <input class="form-control" type="number" name="sale" value="{{$product->sale}}"
                                       required/>
                            </div>
                            <div class="form-group">
                                <label>Quantity</label>
                                <input class="form-control" type="number" name="quantity" value="{{$product->quantity}}"
                                       required/>
                            </div>
                            <button type="submit" class="btn btn-default">Edit</button>
                            <button type="reset" class="btn btn-default">Reset</button>
                        </form>
                    </div>
                @else
                    <h2 class="text-center">This product is not exist.</h2>
                @endif
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </div>
    <!-- /#page-wrapper -->
@stop