@extends('admin.layout.index')
@section('content')
    <!-- Page Content -->
    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Producer
                        <small>List producer</small>
                    </h1>
                    @if(session('msg'))
                        <div class="alert alert-success">
                            {{session('msg')}}
                        </div>
                    @endif
                </div>
                @if(isset($producers) && count($producers)>0)
                    <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                        <tr align="center">
                            <th>ID</th>
                            <th>Producer Name</th>
                            <th>Delete</th>
                            <th>Edit</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($producers as $item)
                            <tr class="odd gradeX" align="center">
                                <td>{{$item->id}}</td>
                                <td>{{$item->name}}</td>
                                <td class="center"><i class="fa fa-trash-o  fa-fw"></i>
                                    <a href="{{url('admin/producer/delete',$item->id)}}"
                                       onclick="return confirm('Are you sure you want to delete this producer?');">
                                        Delete
                                    </a>
                                </td>
                                <td class="center"><i class="fa fa-pencil fa-fw"></i>
                                    <a href="{{url('admin/producer/edit',$item->id)}}">
                                        Edit
                                    </a>
                                </td>
                            </tr>
                        </tbody>
                        @endforeach
                    </table>
                @else
                    <h2>Don't have any producer</h2>
                @endif
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </div>
    <!-- /#page-wrapper -->

@stop