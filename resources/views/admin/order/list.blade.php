@extends('admin.layout.index')
@section('content')
    <!-- Page Content -->
    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Order
                        <small>List</small>
                    </h1>
                </div>
                <!-- /.col-lg-12 -->
                <table class="table table-striped table-bordered table-hover table-responsive" id="dataTables-example"
                       data-url="{{url('admin/order/confirm-order')}}">
                    <thead>
                    <tr align="center">
                        <th>STT</th>
                        <th>Code</th>
                        <th>Customer name</th>
                        <th>Address</th>
                        <th>Phone</th>
                        <th>Total price</th>
                        <th>Date</th>
                        <th>Status</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php if (!empty($list_order)){
                    $i = 1;
                    foreach ($list_order as $order){
                    ?>
                    <tr class="odd gradeX" align="center">
                        <td><?php echo $i; ?></td>
                        <td>
                            <a href="{{url('admin/order/detail',$order->id)}}">{{$order->order_code}}</a>
                        </td>
                        <td>{{$order->user_name}}</td>
                        <td>{{$order->address}}</td>
                        <td>{{$order->phone}}</td>
                        <td>{{number_format($order->total_price)}} đ</td>
                        <td>{{$order->created_at}}</td>
                        <td>
                            <?php if ($order->status == 0) { ?>
                            <span class="btn btn-primary e-confirm-order-trans" data-order="{{$order->id}}">Xác nhận giao hàng</span>
                            <?php } else {
                                echo "Đã giao hàng";
                            } ?>
                        </td>
                    </tr>
                    </tbody>
                    <?php
                    $i++;
                    }
                    } ?>
                </table>
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </div>
    <!-- /#page-wrapper -->
@stop