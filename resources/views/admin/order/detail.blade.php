@extends('admin.layout.index')
@section('content')
    <!-- Page Content -->
    <div id="page-wrapper">
        <div class="container-fluid">
            <?php if(isset($order)){?>
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Order
                        <small>
                            Order detail- {{$order->order_code}}
                        </small>
                    </h1>
                </div>
                <!-- /.col-lg-12 -->
                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                    <thead>
                    <tr align="center">
                        <th> STT</th>
                        <th> Product name</th>
                        <th> Image</th>
                        <th> Price</th>
                        <th> Count</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php if (!empty($order_details)){
                    $i = 1;
                    foreach ($order_details as $detail){?>
                    <tr class="odd gradeX" align="center">
                        <td>{{$i}}</td>
                        <td>{{$detail->product->name}}</td>
                        <td><img src="data:image/png;base64,{{$detail->product->image}}"
                                 alt="{{$detail->product->name}}"
                                 height="100px"></td>
                        <td>{{$detail->price}}</td>
                        <td>{{$detail->count}}</td>
                    </tr>
                    </tbody>
                    <?php
                    $i++;
                    }
                    }?>
                </table>
            </div>
            <!-- /.row -->
            <?php }else{?>
            <h2 class="text-center alert-danger">Không tồn tại đơn hàng.</h2>
            <?php }?>
        </div>
        <!-- /.container-fluid -->
    </div>
    <!-- /#page-wrapper -->
@stop