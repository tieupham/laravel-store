<nav class="navbar navbar-default nav-style">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                    data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="{{url("/")}}"><b>STORE</b></a>
        </div>
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                @if (isset($menu))
                    @foreach ($menu as $item)
                        <li>
                            <a href="{{url('category',$item->id)}}">
                                <b class="text-uppercase"><?php echo $item->name ?></b>
                            </a>
                        </li>
                    @endforeach
                @endif
                <li><a href="{{url("/")}}"><b>TIN TỨC</b></a></li>
            </ul>
            <form action="{{url("search")}}" method="POST" class="navbar-form navbar-left">
                <input type="hidden" name="_token" value="{{csrf_token()}}">
                <div class="form-group">
                    <input type="text" class="form-control" name="key_word" placeholder="Search">
                </div>
                <button type="submit" name="submit" class="btn btn-default"><i class="glyphicon glyphicon-search"></i>
                </button>
            </form>
            <ul class="nav navbar-nav navbar-right">
                @if(Auth::check())
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
                           aria-expanded="false"><span
                                    class="glyphicon glyphicon-user"></span><b>
                                {{Auth::user()->name}}</b> <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="{{url('/logout')}}">logout</a></li>
                            <li><a href="{{url('cart/history')}}">lịch sử GD</a></li>
                        </ul>
                    </li>
                @else
                    <li><a href="{{url('/login')}}"><span class="glyphicon glyphicon-log-in"></span><b>
                                login</b></a></li>
                @endif
                <li><a href="{{url('cart/shopping-cart')}}"><i
                                class="glyphicon glyphicon-shopping-cart"></i><span
                                class="label label-info count-cart-item">{{Session::has('cart')? Session::get('cart')->totalQty:''}}</span>
                    </a>
                </li>
            </ul>
        </div>
    </div>
</nav>