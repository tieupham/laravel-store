@extends('front.layout.index')

@section('content')
    <div class="page-header __history_order" data-ajax-url="{{url('cart/cancel-order')}}">
        <h3>LỊCH SỬ GIAO DỊCH</h3>
        @if(Auth::check())
            @if (isset($orders) && (count($orders)>0))
                @foreach ($orders as $key => $order)
                    @if (!empty($order->orderDetail))
                        <div class="row">
                            <div class="panel panel-info">
                                <div class="panel-heading">
                                    <h4 class="panel-title">{{$order->user_name . ' - ' .
                        date('d/m/Y', strtotime($order->created_at)) . ' - ' .
                        number_format($order->total_price) . 'đ'}}
                                        @if ($order->status ==0)
                                            <span class="btn btn-danger pull-right e-cancel-order"
                                                  data-order-id="{{$order->id}}">Hủy
                                    </span>
                                        @endif
                                    </h4>

                                </div>
                                <div class="panel-body">
                                    @foreach ($order->orderDetail as $detail)
                                        <div class="row">
                                            <div class="col-sm-7">
                                                <div class="col-sm-4">
                                                    <img class="img-responsive center-block"
                                                         src="data:image/png;base64,{{$detail->product->image}}"
                                                         alt="{{$detail->product->name}}" height="90%">
                                                </div>
                                                <div class="col-sm-8">
                                                    <a href="{{url('product',$detail->product_id)}}">{{$detail->product->name}}</a>
                                                </div>
                                            </div>
                                            <div class="col-sm-5">
                                                <div class="col-sm-6">
                                                    <strong>{{number_format($detail->price) . ' đ'}}</strong>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div>
                                                        <input class="count-item form-control" disabled
                                                               value="{{$detail->count }}">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <hr>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    @endif
                @endforeach
            @else
                <h4 class="text-center">Không có giao dịch nào</h4>
            @endif
        @else
            <h4 class="text-center">Bạn cần phải đăng nhập để xem lịch sử</h4>
        @endif

    </div>
@stop