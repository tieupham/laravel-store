@extends('front.layout.index')

@section('content')
    <div class="row">
        <a href="{{url('cart/history')}}" class="link-text" role="button">
            Xem lịch sử giao dịch</a>
    </div>
    <hr>
    @if (Session::has('cart'))
        <div class="col-lg-9">
            <div class="panel panel-default e-data-url"
                 data-url-delete="{{url('cart/delete-cart-item')}}"
                 data-url-update="{{url('cart/change-item-count')}}" style="margin-bottom: 100px;">

                <div class="panel-heading">
                    <h4>GIỎ HÀNG</h4>
                </div>
                <div class="panel-body">
                    @foreach ($products as $product)
                        <div class="row">
                            <div class="col-sm-7">
                                <div class="col-sm-4">
                                    <img class="img-responsive center-block"
                                         src="data:image/png;base64,{{$product['item']['image']}}"
                                         alt="{{$product['item']['name']}}" height="90%">
                                </div>
                                <div class="col-sm-8">
                                    <a href="{{url('product',$product['item']['id'])}}">{{$product['item']['name']}}</a>
                                    <h5>
                                        Cung cấp bởi <a href="{{url('/')}}">STORE</a>
                                    </h5>
                                    <span class="btn btn-danger e-delete-cart-item"
                                          data-id="{{$product['item']['id']}}">Xóa
                                    </span>
                                </div>
                            </div>
                            <div class="col-sm-5">
                                <div class="col-sm-6">
                                    <strong>{{number_format($product['item']['price']) . ' đ'}}</strong>
                                    <p class="badge">- {{$product['item']['sale']}}%</p>
                                </div>
                                <div class="col-sm-6">
                                    <div>
                                        <input class="count-item form-control data-count-{{$product['item']['id']}}"
                                               type="number" value="{{$product['qty']}}">
                                        <small>
                                            <button class="btn btn-primary e-update-cart"
                                                    data-id="{{$product['item']['id']}}">Cập nhật
                                            </button>
                                        </small>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr>
                    @endforeach
                </div>

            </div>
        </div>
        <div class="col-lg-3">
            <div class="panel panel-default">
                <div class="panel-heading">ĐƠN HÀNG</div>
                <div class="panel-body">
                    <h4>Thành tiền
                        <b>{{number_format($totalPrice)}} đ</b>
                    </h4>
                </div>
            </div>

            <a href="{{url('cart/checkout')}}" class="btn btn-primary btn-block">
                Tiến hành đặt hàng
            </a>
        </div>
    @else
        <h2 class="text-center">Giỏ hàng không có sản phẩm nào.</h2>
    @endif
@stop
