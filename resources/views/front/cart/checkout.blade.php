@extends('front.layout.index')

@section('content')
    @if(Session::has('cart'))
        <div class="container" style="max-width: 500px;">
            <div class="login-panel panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Thông tin đơn hàng</h3>
                </div>
                <div class="panel-body">
                    @if(count($errors)>0)
                        <div class="alert alert-danger">
                            @foreach($errors->all() as $err)
                                {{$err}}<br>
                            @endforeach
                        </div>
                    @endif
                    <form action="" method="POST">
                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                        @if(Auth::check())
                            <div class="form-group">
                                <label style="color: #ff4422">(*)Thanh toán tại nhà</label>
                            </div>
                            <button type="submit" class="btn btn-default" name="submit">Xác nhận</button>
                        @else
                            <div class="form-group">
                                <label>Tên đầy đủ</label>
                                <input class="form-control" required name="user_name"/>
                            </div>
                            <div class="form-group">
                                <label>Địa chỉ</label>
                                <input class="form-control" required name="address"/>
                            </div>
                            <div class="form-group">
                                <label>Số điện thoại</label>
                                <input type="number" class="form-control" required name="phone"/>
                            </div>
                            <div class="form-group">
                                <label style="color: #ff4422">(*)Thanh toán tại nhà</label>
                            </div>
                            <button type="submit" class="btn btn-default" name="submit">Xác nhận</button>
                            <button type="reset" class="btn btn-default">Reset</button>
                        @endif
                    </form>
                </div>
            </div>
        </div>
    @else
        <h2 class="text-center">Giỏ hàng không có sản phẩm nào.</h2>
    @endif
@stop