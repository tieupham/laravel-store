@extends('front.layout.index')
@section('content')
    @if(session('msg'))
        <div class="alert alert-success">
            {{session('msg')}}
        </div>
    @endif
    <div class="row">
        <div class="col-sm-8">

        </div>
        <div class="col-sm-4">

        </div>
    </div>
    <div class="row">
        @if(isset($list_products))
            @foreach ($list_products as $product)
                <a href="{{url('product',$product->id)}}">
                    <div class="col-sm-4 product-thumb">
                        <div class="thumbnail">
                            <img class="img-responsive" src="data:image/png;base64,{{$product->image}}"
                                 alt="{{$product->name}}" width="50%">
                            <div class="caption">
                                <h4>{{$product->name}}</h4>
                                @php
                                    if ($product->sale > 0) {
                                        $price = $product->price - ($product->price * $product->sale) / 100;
                                        $o_price = $product->price;
                                    } else {
                                        $price = $product->price;
                                    }
                                @endphp
                                <strong>{{number_format($price) . ' đ'}}</strong>
                                <p class="old-price">{{(isset($o_price))? (number_format($o_price) . ' đ') : ""}}</p>
                            </div>
                        </div>
                    </div>
                </a>
            @endforeach
        @endif
    </div>
    </div>
    @if (!empty($accessory_products))
        <div class="acchome">
            <div class="naviacc">
                <h2>Phụ kiện giá rẻ</h2>
                @if (!empty($list_accessories))
                    @foreach ($list_accessories as $accessory)
                        <a href="{{url('category/index/',$accessory->id)}}">{{$accessory->name}}</a>
                    @endforeach
                @endif
            </div>
            <div class="row">
                @foreach ($accessory_products as $product)
                    <a href="{{url('product',$product->id)}}">
                        <div class="col-sm-1-5">
                            <div class="thumbnail">
                                <img class="img-responsive" src="data:image/png;base64,{{$product->image}}"
                                     alt="{{$product->name}}" width="65%" height="40%">
                                <div class="caption text-center">
                                    <h5>{{$product->name}}</h5>
                                    @php
                                        if ($product->sale > 0) {
                                            $price = $product->price - ($product->price * $product->sale) / 100;
                                            $o_price = $product->price;
                                        } else {
                                            $price = $product->price;
                                        }
                                    @endphp
                                    <strong>{{number_format($price) . ' đ'}}</strong>
                                    <h6>{{(isset($o_price))? (number_format($o_price) . ' đ') : ""}}</h6>
                                </div>
                            </div>
                        </div>
                    </a>
                @endforeach
            </div>
        </div>
    @endif
@stop