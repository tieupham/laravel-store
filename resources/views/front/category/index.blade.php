@extends('front.layout.index')

@section('content')
    @if(!empty($list_cat_child))
        <div class="row">
            <ol class="breadcrumb">
                @foreach ($list_cat_child as $cat)
                <li>
                    <a href="{{url('category',$cat->id)}}">
                        {{$cat->name}}
                    </a>
                </li>
                @endforeach
            </ol>
        </div>
    @endif

    <div class="row">
        @if (!empty($list_products))
            <div class="panel panel-default">

                <div class="panel-heading">
                    <h4>
                        {{$category_name->name}}
                    </h4>
                </div>
                <div class="panel-body container data-load-more">
                    @foreach ($list_products as $product)
                    <a href="{{url('product',$product->id)}}">
                        <div class="col-sm-2-4 product-thumb">
                            <div class="thumbnail">
                                <img class="img-responsive" src="data:image/png;base64,{{$product->image}}"
                                     alt="{{$product->name}}" width="50%">
                                <div class="caption">
                                    <h4>{{$product->name}}</h4>
                                    @php
                                        if ($product->sale > 0){
                                            $price = $product->price - ($product->price * $product->sale) / 100;
                                            $o_price = $product->price;
                                        }else{
                                            $price = $product->price;
                                        }
                                    @endphp
                                    <strong>{{number_format($price) . ' đ'}}</strong>
                                    <p class="old-price">
                                        @php
                                            if (isset($o_price)) {
                                                    echo number_format($o_price) . ' đ';
                                                }
                                        @endphp
                                    </p>
                                </div>
                            </div>
                        </div>
                    </a>
                    @endforeach
                </div>
                <div class="container">
                    {{ $list_products->links() }}
                </div>
            </div>
        @endif
    </div>
@endsection