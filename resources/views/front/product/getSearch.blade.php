@extends('front.layout.index')

@section('content')
    <div class="row container">
        <form action="{{url("search")}}" method="POST" class="center-block">
            <h3>Nhập từ khóa cần tìm kiếm</h3>
            <input type="hidden" name="_token" value="{{csrf_token()}}">
            <div class="form-group">
                <input type="text" class="form-control" name="key_word" placeholder="Search">
            </div>
            <button type="submit" name="submit" class="btn btn-default">Search
            </button>
        </form>
    </div>
@endsection