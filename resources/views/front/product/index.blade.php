@extends('front/layout/index')
@section('content')
    @if (isset($product))
        <div class="page-header">
            <h1>{{$product->name}}</h1>
        </div>
        <div class="row">
            <div class="col-sm-6">
                <img class="img-responsive center-block" src="data:image/png;base64,{{$product->image}}"
                     alt="{{$product->name}}" width="80%"/>
            </div>
            <div class="col-sm-6">
                <div class="col-sm-6 center-block">
                    <div class="product-status">
                        <h5>Giá sản phẩm</h5>
                        @php
                            if ($product->sale > 0) {
                                $price = $product->price - ($product->price * $product->sale) / 100;
                                $o_price = $product->price;
                            } else {
                                $price = $product->price;
                            }
                        @endphp
                        <strong>{{number_format($price)}}<sup> đ</sup></strong>
                        @php if (isset($o_price)) {echo "<span>" . number_format($o_price) . "<sup> đ</sup></span>";} @endphp
                        <h5>Nhà sản xuất</h5>
                        <b>{{$product->producer->name}}</b>
                        <h5>Tình trạng hàng</h5>
                        <strong>
                            {{$product->quantity > 0 ? "Còn hàng" : "Tạm thời hết hàng"}}
                        </strong>
                    </div>
                    <div class="btn-product">
                        <button class=" text-center btn btn-primary btn-block" role="button">Mua hàng</button>
                        <a class="text-center btn btn-primary btn-block e_add_to_cart" role="button"
                           href="{{url('cart/add',$product->id)}}">Thêm vào giỏ hàng
                        </a>
                    </div>
                </div>
                <div class="col-sm-6">
                    //làm gì đó
                </div>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-md-8">
                <div class="page-header">
                    <h4>MÔ TẢ SẢN PHẨM</h4>
                </div>
                <p>{{$product->description}}</p>
            </div>
            <div class="col-md-4">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h4 class="panel-title">TIN TỨC</h4>
                    </div>
                    <div class="panel-body">
                        @for($i=0;$i<3;$i++)
                            <div class="row">
                                <div class="col-sm-4">
                                    <img class="img-responsive center-block"
                                         src="{{asset('image/320x150.png')}}"
                                         width="95%"/>
                                </div>
                                <div class="col-sm-8">
                                    <p>"Không có ai muốn khổ đau cho chính mình, muốn tìm kiếm về nó và muốn có nó, bởi
                                        vì
                                        nó là
                                        sự
                                        đau khổ..."</p>
                                </div>
                            </div>
                            <hr>
                        @endfor
                    </div>
                </div>

            </div>
        </div>
        <div class="row">
            <div class="panel panel-info">
                <div class="panel-heading"><h4 class="panel-title">SẢN PHẨM LIÊN QUAN</h4></div>
                <div class="panel-body">
                    @if (isset($product_involve) && count($product_involve)>0)
                        @foreach ($product_involve as $item)
                            <a href="{{url('product',$item->id)}}">
                                <div class="col-sm-2-4">
                                    <div class="thumbnail">
                                        <img class="img-responsive" src="data:image/png;base64,{{$item->image}}"
                                             alt="{{$item->name}}" width="65%" height="40%">
                                        <div class="caption text-center">
                                            <h5>{{$item['name']}}</h5>
                                            @php
                                                if ($item['sale'] > 0) {
                                                    $price = $item->price - ($item->price * $item->sale) / 100;
                                                    $o_price = $item->price;
                                                } else {
                                                    $price = $item->price;
                                                }
                                            @endphp
                                            <strong>{{number_format($price) . ' đ'}}</strong>
                                            <h6>{{(isset($o_price))? (number_format($o_price) . ' đ') : ""}}</h6>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        @endforeach
                    @endif
                </div>
            </div>
        </div>
        {{--Binh luan--}}

    @else
        <div class="page-header text-center">
            <h1>Sản phẩm không tồn tại</h1>
        </div>
    @endif
@stop