@extends('front.layout.index')

@section('content')
    <div class="row">
        @if (!empty($products))
            <div class="panel-heading">
                <h4>Có {{count($products)}} sản phẩm tìm thấy với từ khóa
                    <i>" {{$key_word}}"</i>
                </h4>
            </div>
            <div class="panel panel-default" style="margin-bottom: 100px;">
                <div class="panel-body container data-load-more">
                    @foreach ($products as $product)
                        <a href="{{url('product',$product->id)}}">
                            <div class="col-sm-2-4 product-thumb">
                                <div class="thumbnail">
                                    <img class="img-responsive" src="data:image/png;base64,{{$product->image}}"
                                         alt="{{$product->name}}" width="50%">
                                    <div class="caption">
                                        <h4>{{$product->name}}</h4>
                                        @php
                                            if ($product->sale > 0){
                                                $price = $product->price - ($product->price * $product->sale) / 100;
                                                $o_price = $product->price;
                                            }else{
                                                $price = $product->price;
                                            }
                                        @endphp
                                        <strong>{{number_format($price) . ' đ'}}</strong>
                                        <p class="old-price">@php if (isset($o_price)) {echo number_format($o_price) . ' đ';} @endphp</p>
                                    </div>
                                </div>
                            </div>
                        </a>
                    @endforeach
                </div>
            </div>
        @else
            <h3 class="text-center">Không tìm thấy sản phẩm nào.</h3>
        @endif
    </div>
@endsection