$(document).ready(function () {
    $(document).on('click', 'span.e-confirm-order-trans', confirm_order);

    function confirm_order(e) {
        if (confirm("Bạn có đồng ý xác nhận đơn hàng này đã giao thành công không ?")) {
            var order_id = $(this).attr('data-order');
            var url = $('table').attr('data-url');
            var currentToken = $('meta[name="csrf-token"]').attr('content');
            $.ajax(
                {
                    url     : url,
                    type    : "POST",
                    data    : {'order_id': order_id, _token: currentToken},
                    dataType: "json",
                    success : function (data) {
                        if (data['status']) {
                            location.reload();
                        } else {
                            console.log(data['message']);
                        }

                    },
                    error   : function (a, b, c) {
                        console.log(a, b, c);
                    },
                    complete: function (jqXHR, textStatus) {
                    }
                }
            );
        } else {
            e.preventDefault();
        }
    }
});